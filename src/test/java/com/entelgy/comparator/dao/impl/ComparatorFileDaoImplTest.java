package com.entelgy.comparator.dao.impl;

import com.entelgy.comparator.EntityMock;
import com.entelgy.comparator.bean.ProcessBean;
import com.entelgy.comparator.util.FileUtil;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;


public class ComparatorFileDaoImplTest {

    @InjectMocks
    private ComparatorFileDaoImpl comparatorFileDao;

    @Mock
    private FileUtil fileUtil;

    @Before
    public void setUp() {
        fileUtil = FileUtil.getInstance();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getListFileProcessFullTest(){
        String path= "Path";
        Mockito.when( fileUtil.getListFilesPath(path))
                .thenReturn(EntityMock.getInstance().build_ListContentFile());

        ProcessBean processBean = comparatorFileDao.getListFileProcess(path);
        assertNotNull(processBean);
        assertNotNull(processBean.getListFiles());
        assertEquals(2, processBean.getListFiles().size() );
        assertNull(processBean.getFile1());
        assertNull(processBean.getFile2());
    }
}
