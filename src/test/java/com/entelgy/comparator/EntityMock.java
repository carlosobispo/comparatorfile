package com.entelgy.comparator;

import java.util.ArrayList;

public class EntityMock {

    private static final  EntityMock INSTANCE = new EntityMock();

    private EntityMock(){

    }

    public static EntityMock getInstance(){
        return INSTANCE;
    }


    public ArrayList<String> build_ListContentFile(){
        ArrayList<String> listFiles = new ArrayList<String>();
        listFiles.add("produccion.txt");
        listFiles.add("testing.txt");
        listFiles.add("app");
        listFiles.add("log");
        return listFiles;
    }



}
