package com.entelgy.comparator.fachada;

public interface IComparatorFile {

    void processFile();
}
