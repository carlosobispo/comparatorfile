package com.entelgy.comparator.fachada.impl;

import com.entelgy.comparator.bean.ProcessBean;
import com.entelgy.comparator.util.bundle.ResourceBundleUtil;
import com.entelgy.comparator.dao.IComparatorFileDao;
import com.entelgy.comparator.dao.impl.ComparatorFileDaoImpl;
import com.entelgy.comparator.fachada.IComparatorFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ComparatorFile implements IComparatorFile{

    private static final Logger LOG = LoggerFactory.getLogger(ComparatorFile.class);


    public void processFile() {

        try {
            LOG.info("... invoke ComparatorFile.processFile ...");
            String path_file = ResourceBundleUtil.getResourceValue("path_file");
            LOG.info("ruta de archivos a comparar : "+ path_file);

            IComparatorFileDao comparatorFileDao = new ComparatorFileDaoImpl();
            ProcessBean processBean = comparatorFileDao.getListFileProcess(path_file);
            comparatorFileDao.generateComparation(processBean);

        }catch (Exception e2) {
            LOG.error("ERROR General, " ,e2);
        }
    }



}
