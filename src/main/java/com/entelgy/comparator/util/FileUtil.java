package com.entelgy.comparator.util;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FileUtil {
	
	private static final Logger LOG =LoggerFactory.getLogger( FileUtil.class);
	
	private static final FileUtil INSTANCE = new FileUtil();
	
	private FileUtil(){
	}
	
	public static FileUtil getInstance(){
		return INSTANCE;
	}
	
	public void generateFileResult( String ruta, StringBuilder builder){
		
		LOG.info("... invoke FileUtil.generateFileResult ... ");
		LOG.info("..archivo resultado a crear : "+ ruta);
		
		try {
			File path_result = new File(ruta);
			path_result.mkdirs();
			File file_result = new File(ruta + "/result.txt");
	        BufferedWriter bw = new BufferedWriter(new FileWriter(file_result));
            bw.write("");
            bw.append(builder.toString());
	        bw.close();	
		} catch (Exception e) {
			LOG.error(" Error al crear FileResult ", e);
		}
	}


	public ArrayList<String> getListContentFile(File archivo) {
		LOG.info("... invoke FileUtil.getListContentFile ... ");
		ArrayList<String> listContent = new ArrayList<String>();
		try {
			String cadena;
			FileReader f = new FileReader(archivo);
			BufferedReader b = new BufferedReader(f);
			while((cadena = b.readLine())!=null) {
				listContent.add(cadena);
			}
			b.close();
		}catch (FileNotFoundException e1){
			LOG.error("Archivo no encontrado " + archivo , e1);
		}catch ( IOException e2){
			LOG.error("Error al procesar archivo ", e2);
		}
		return listContent;
	}

	public List<String> getListFilesPath(String path){
		LOG.info("... invoke FileUtil.getListFilesPath ... ");
		File mainFolder = new File(path);
		String[] arrayFiles =  mainFolder.list();
		if(arrayFiles==null){
			LOG.warn("No se encontraron archivos para realizar la comparacion");
			return null;
		}
		return  Arrays.asList( arrayFiles );
	}

	public boolean copyFile(File fromFile, File toFile) {
		LOG.info("... invoke FileUtil.copyFile ... ");
		if (fromFile.exists()) {
			try {
				InputStream in = new FileInputStream(fromFile);
				OutputStream out = new FileOutputStream(toFile);
				// We use a buffer for the copy (Usamos un buffer para la copia).
				byte[] buf = new byte[1024];
				int len;
				while ((len = in.read(buf)) > 0) {
					out.write(buf, 0, len);
				}
				in.close();
				out.close();
				return true;
			} catch (IOException ioe) {

				ioe.printStackTrace();
				return false;
			}
		} else {
			LOG.info("No existe la ruta fromFile : "+ fromFile.getName());
			return false;
		}
	}

	
}
