package com.entelgy.comparator.dao.impl;

import com.entelgy.comparator.bean.ProcessBean;
import com.entelgy.comparator.business.IComparatorFileBusiness;
import com.entelgy.comparator.util.bundle.ResourceBundleUtil;
import com.entelgy.comparator.business.impl.ComparatorFileBusiness;
import com.entelgy.comparator.dao.IComparatorFileDao;
import com.entelgy.comparator.util.FileUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ComparatorFileDaoImpl implements IComparatorFileDao{

    private static final Logger LOG = LoggerFactory.getLogger(ComparatorFileDaoImpl.class);

    private FileUtil fileUtil = FileUtil.getInstance();

    public ProcessBean getListFileProcess(String path_process) {
        LOG.info("... call ComparatorFileDaoImpl.getListFileProcess ...");

        List<String> listFiles = fileUtil.getListFilesPath(path_process);
        List<String> listFilesProcess = new ArrayList<String>();
        for(String file : listFiles){
            if( file.lastIndexOf(".")< 0) { continue; }
            String file_extension = file.substring( file.lastIndexOf(".") );
            if(".txt".equals( file_extension )){
                LOG.info("archivo valido a procesar : " + file);
                listFilesProcess.add(file);
            }

        }
        LOG.info("lista de archivos encontrados " + listFiles.toString());
        ProcessBean processBean= new ProcessBean();
        processBean.setListFiles( listFilesProcess);
        return processBean;
    }

    public void generateComparation(ProcessBean processBean) {
        LOG.info("... call ComparatorFileDaoImpl.generateComparation ...");
        if( !isValidListFilesProcess(processBean) ){
            return;
        }
        LOG.info("Se realizo la validacion, todo correcto");
        IComparatorFileBusiness fileBusiness = new ComparatorFileBusiness();
        StringBuilder stringBuilder = fileBusiness.comparationFile(processBean);

        String path_file_result = ResourceBundleUtil.getResourceValue("path_file_comparation_result");
        fileUtil.generateFileResult(path_file_result, stringBuilder);
    }

    private boolean isValidListFilesProcess(ProcessBean processBean){
        if(processBean==null || CollectionUtils.isEmpty(processBean.getListFiles())){
            LOG.warn("List de archivos is Empty");
            return false;
        }
        if( processBean.getListFiles().size() != 2 ){
            LOG.warn("Solo debe existir 2 archivos .txt para realizar la comparacion");
            return false;
        }
        String path_file = ResourceBundleUtil.getResourceValue("path_file");

        String fileProcess1 = path_file + "/" + processBean.getListFiles().get(0);
        String fileProcess2 = path_file + "/" + processBean.getListFiles().get(1);

        processBean.setFile1( new File( fileProcess1) );
        processBean.setFile2( new File( fileProcess2) );
        return true;
    }

}
