package com.entelgy.comparator.dao;

import com.entelgy.comparator.bean.ProcessBean;

public interface IComparatorFileDao {

    ProcessBean getListFileProcess(String path_process);
    void generateComparation( ProcessBean processBean);
}
