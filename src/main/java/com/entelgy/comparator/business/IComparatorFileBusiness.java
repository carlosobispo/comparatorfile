package com.entelgy.comparator.business;

import com.entelgy.comparator.bean.ProcessBean;

public interface IComparatorFileBusiness {
    StringBuilder comparationFile(ProcessBean processBean);
}
