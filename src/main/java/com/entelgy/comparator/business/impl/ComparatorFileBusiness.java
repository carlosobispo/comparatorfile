package com.entelgy.comparator.business.impl;

import com.entelgy.comparator.bean.ProcessBean;
import com.entelgy.comparator.business.IComparatorFileBusiness;
import com.entelgy.comparator.util.FileUtil;
import com.entelgy.comparator.util.bundle.ResourceBundleUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.ArrayList;

public class ComparatorFileBusiness
        implements IComparatorFileBusiness {

    private static final Logger LOG = LoggerFactory.getLogger( ComparatorFileBusiness.class);

    public StringBuilder comparationFile(ProcessBean processBean) {
        LOG.info("... invoke ComparatorFileBusiness.comparationFile ...");
        String path_temp = ResourceBundleUtil.getResourceValue("path_temp");
        copyFileTemporary(processBean,path_temp);

        FileUtil fileUtil = FileUtil.getInstance();
        ArrayList<String> listContentFile1 = fileUtil.getListContentFile(processBean.getFile1());
        ArrayList<String> listContentFile2 = fileUtil.getListContentFile(processBean.getFile2());

        StringBuilder stringBuilder = new StringBuilder("");

        LOG.info("Se realiza la comparacion de "+ processBean.getFile1().getName()+ " --> "+ processBean.getFile2().getName());
        stringBuilder.append("PROPIEDADES FALTANTES EN : "+ processBean.getFile2().getName() + getSaltoLinea());
        getDifferencesFile(stringBuilder, listContentFile1, listContentFile2);
        LOG.info("=====================================================================");
        LOG.info("Se realiza la comparacion de "+ processBean.getFile1().getName()+ " --> "+ processBean.getFile2().getName());
        stringBuilder.append("PROPIEDADES FALTANTES EN : "+ processBean.getFile1().getName() + getSaltoLinea());
        getDifferencesFile(stringBuilder, listContentFile2, listContentFile1);

        return stringBuilder;
    }

    private void getDifferencesFile(StringBuilder stringBuilder, ArrayList<String> listContentFile1,
                                    ArrayList<String> listContentFile2){

        LOG.info("... invoke ComparatorFileBusiness.getDifferencesFile ...");
        ArrayList<String> listContentFile1_clone = ((ArrayList<String>) listContentFile1.clone());
        ArrayList<String> listContentFile2_clone = ((ArrayList<String>) listContentFile2.clone());
        if(CollectionUtils.isEmpty(listContentFile1_clone)){
            return;
        }

        if(CollectionUtils.isEmpty(listContentFile2_clone)){
            LOG.info("No existe contenido en el archivo de comparacion, se procede a copiar todo su contenido");
            for(String content_file1 : listContentFile1_clone){
                stringBuilder.append(content_file1 + getSaltoLinea());
            }
            return;
        }

        for(String contentFile2 :listContentFile2){
            contentFile2 = contentFile2.replace(" ","");
            if(StringUtils.isEmpty(contentFile2)){ continue; }
            for(int i=0; i<listContentFile1_clone.size(); i++ ){
                String contentFile1 = listContentFile1_clone.get(i).replace(" ","");
                if( contentFile2.equals(contentFile1) ){
                    listContentFile1_clone.remove(i);
                    i--;
                    break;
                }

            }
        }

        if(CollectionUtils.isEmpty(listContentFile1_clone)){
            LOG.info("No se encontro diferencias ");
            return;
        }
        for(String content1 : listContentFile1_clone){
            content1 = content1.replace(" ","");
            if(StringUtils.isEmpty(content1)){ continue; }
            stringBuilder.append(content1 + getSaltoLinea());
        }
        stringBuilder.append( getSaltoLinea() + getSaltoLinea());
    }

    public String getSaltoLinea(){
        return  ResourceBundleUtil.getResourceValue("line.separator");
    }


    private void copyFileTemporary(ProcessBean processBean, String path_temporary){
        LOG.info("... invoke ComparatorFileBusiness.copyFileTemporary ...");
        File file_path_temporary= new File(path_temporary);
        file_path_temporary.mkdirs();

        String file_temporary1 = path_temporary + "/" + processBean.getFile1().getName();
        LOG.info("archivo temporal1 "+ file_temporary1);

        String file_temporary2 = path_temporary + "/" + processBean.getFile2().getName();
        LOG.info("archivo temporal2 "+ file_temporary2);

        File fileTemporary1 = new File(file_temporary1);
        File fileTemporary2 = new File(file_temporary2);

        FileUtil fileUtil = FileUtil.getInstance();

        if( fileUtil.copyFile( processBean.getFile1(), fileTemporary1 ) ) {
            processBean.setFile1(fileTemporary1);
        }
        if( fileUtil.copyFile( processBean.getFile2(), fileTemporary2 ) ) {
            processBean.setFile2(fileTemporary2);
        }
    }



}
