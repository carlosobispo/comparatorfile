package com.entelgy.comparator.bean;

import java.io.File;
import java.io.Serializable;
import java.util.List;

public class ProcessBean implements Serializable{

    private static final long serialVersionUID = 1L;

    private List<String> listFiles;
    private File file1;
    private File file2;


    public List<String> getListFiles() {
        return listFiles;
    }

    public void setListFiles(List<String> listFiles) {
        this.listFiles = listFiles;
    }

    public File getFile1() {
        return file1;
    }

    public void setFile1(File file1) {
        this.file1 = file1;
    }

    public File getFile2() {
        return file2;
    }

    public void setFile2(File file2) {
        this.file2 = file2;
    }
}
