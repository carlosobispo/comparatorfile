package com.entelgy.comparator.principal;

import com.entelgy.comparator.fachada.IComparatorFile;
import com.entelgy.comparator.fachada.impl.ComparatorFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AppPrincipal {
	
	private static final Logger LOG =LoggerFactory.getLogger(AppPrincipal.class);
	
	public static void main(String[] args) {
		LOG.info("========== Inicio Comparacion de archivos ==========");
		IComparatorFile comparatorFile= new ComparatorFile();
		comparatorFile.processFile();
	}
	
}
